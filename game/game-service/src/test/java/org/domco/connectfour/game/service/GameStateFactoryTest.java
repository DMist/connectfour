package org.domco.connectfour.game.service;

import org.domco.connectfour.game.domain.GameState;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Game state factory test.
 */
public class GameStateFactoryTest {

    /** Class under test. */
    private GameStateFactory gameStateFactory;

    /**
     * Width of a standard game.
     * For standard rules set equal to 7
     */
    private static final int STANDARD_GAME_WIDTH = 7;

    /**
     * Height of a standard game.
     * For standard rules set equal to 6
     */
    private static final int STANDARD_GAME_HEIGHT = 6;

    /**
     * Number of consecutive pieces that are the same colour required for the game to be won.
     * For standard rules set equal to 4
     */
    private static final int CONSECUTIVE_PIECES_FOR_WIN = 4;

    /**
     * The handicap variable dictates how many turns player two gets for each of player ones.
     * When HANDICAP = 0. Player two gets 1 turn for each of player ones turn
     * When HANDICAP = 1. Player two gets 2 turns for each of player ones turn
     * When HANDICAP = 2. Player two gets 3 turns for each of player ones turn
     * etc...
     */
    private static final int HANDICAP = 0;

    @Before
    public void setup() {
        gameStateFactory = new GameStateFactoryHandler();
    }

    /**
     * Checks game state creation.
     */
    @Test
    public void checkCreateEmptyStandardGameState() {

        // Expected
        final int expectedPieceCount = 0;

        // Test
        final GameState gameState = gameStateFactory.createEmptyStandardGameState();

        // Assert
        Assert.assertEquals(STANDARD_GAME_WIDTH, gameState.getGameWidth());
        Assert.assertEquals(STANDARD_GAME_HEIGHT, gameState.getGameHeight());
        Assert.assertEquals(CONSECUTIVE_PIECES_FOR_WIN, gameState.getConsecutivePiecesForWin());
        Assert.assertEquals(HANDICAP, gameState.getPlayerTwoHandicap());
        Assert.assertEquals(expectedPieceCount, gameState.getGamePieceSet()
            .size());
    }
}
