package org.domco.connectfour.server.domain;

import java.util.UUID;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.domco.connectfour.game.domain.PieceColour;

/**
 * Game player.
 */
public final class GamePlayer {

    private final String key;

    private final String name;

    private PieceColour pieceColour;

    private Game game;

    public PlayerMoveResult playMove(final int columnNumber) {

        PlayerMoveResult playerMoveResult;

        if (null == game) {
            playerMoveResult = PlayerMoveResult.GAME_NOT_READY;
        } else {
            playerMoveResult = game.playMove(columnNumber, this);
        }
        return playerMoveResult;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(final Game game) {
        this.game = game;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name;
    }

    public void setPieceColour(final PieceColour pieceColour) {
        this.pieceColour = pieceColour;
    }

    public PieceColour getPieceColour() {
        return pieceColour;
    }


    public GamePlayer(final String name) {

        this.key = UUID.randomUUID().toString();
        this.name = name;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(key)
                .append(name)
                .append(pieceColour)
                .toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        boolean equals = false;
        if (obj instanceof GamePlayer) {
            final GamePlayer rhs = (GamePlayer) obj;
            equals = new EqualsBuilder().append(key, rhs.key)
                    .append(name, rhs.name)
                    .append(pieceColour, rhs.pieceColour)
                    .isEquals();
        }
        return equals;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("key", key)
                .append("name", name)
                .append("pieceColour", pieceColour)
                .toString();
    }
}
