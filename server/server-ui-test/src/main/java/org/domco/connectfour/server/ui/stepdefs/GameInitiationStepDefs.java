package org.domco.connectfour.server.ui.stepdefs;

import com.frameworkium.core.ui.tests.BaseUITest;
import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.domco.connectfour.server.ui.domain.Game;
import org.domco.connectfour.server.ui.domain.GamePlayer;
import org.domco.connectfour.server.ui.service.GameInitiationService;
import org.domco.connectfour.server.ui.service.TestStateContainer;
import org.domco.connectfour.server.ui.stepdefs.mappers.GherkinDataTablesMapper;

import javax.inject.Inject;

/**
 * Step definitions for Game Initiation.
 */
public class GameInitiationStepDefs extends BaseUITest {

    private final TestStateContainer testStateContainer;
    private final GameInitiationService gameInitiationService;
    private final GherkinDataTablesMapper gherkinDataTablesMapper;


    @Inject
    public GameInitiationStepDefs(TestStateContainer testStateContainer, GameInitiationService gameInitiationService,
                                  GherkinDataTablesMapper gherkinDataTablesMapper) {
        this.testStateContainer = testStateContainer;
        this.gameInitiationService = gameInitiationService;
        this.gherkinDataTablesMapper = gherkinDataTablesMapper;
    }

    @When("^A player named \"([^\"]*)\" navigates to the home page$")
    public void aPlayerNamedNavigatesToTheHomePage(String playerName) {
        testStateContainer.setGame(Game.createNewSinglePlayerGameInstance(playerName));
        GamePlayer yellowPlayer = testStateContainer.getGame().getGamePlayerList().get(0);

        gameInitiationService.openHomePage(yellowPlayer);
    }

    @When("^The user selects to play a game$")
    public void theUserSelectsToPlayAGame() {
        GamePlayer yellowPlayer = testStateContainer.getGame().getGamePlayerList().get(0);
        gameInitiationService.requestToPlayAGame(yellowPlayer);
    }

    @Given("^two users \"([^\"]*)\" and \"([^\"]*)\" have chosen to join a game$")
    public void twoUsersAndHaveChosenToJoinAGame(String yellowPlayerName, String redPlayerName) {

        testStateContainer.setGame(Game.createNewTwoPlayerGameInstance(yellowPlayerName, redPlayerName));

        //Red player joins which starts the game and each player gets their game page updated.
        gameInitiationService.bothPlayersJoinToInitiateGame();

    }

    @Then("^They are informed they need to wait for another player to join and the game has the following information$")
    public void theyAreInformedTheyNeedToWaitForAnotherPlayerToJoinAndTheGameHasTheFollowingInformation(
            DataTable expectedGameStateDataTable) {

        Game expectedGame = gherkinDataTablesMapper.mapToExpectedGame(expectedGameStateDataTable);
        gameInitiationService.assertUserHasToWaitForOpponent(expectedGame);

    }

}
