package org.domco.connectfour.server.ui.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Set;

/**
 * Game State
 */
public class GameState {

    private final int gameWidth;

    private final int gameHeight;

    private final PlayerColour colourPlayed;

    private final Set<GamePiece> gamePieceSet;

    private GameState(final GameStateBuilder builder) {
        this.gameWidth = builder.gameWidth;
        this.gameHeight = builder.gameHeight;
        this.colourPlayed = builder.colourPlayed;
        this.gamePieceSet = builder.gamePieceSet;
    }

    public int getGameWidth() {
        return gameWidth;
    }

    public int getGameHeight() {
        return gameHeight;
    }

    public PlayerColour getPieceColour() {
        return colourPlayed;
    }

    public Set<GamePiece> getGamePieceSet() {
        return gamePieceSet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GameState gameState = (GameState) o;

        return new EqualsBuilder()
                .append(gameWidth, gameState.gameWidth)
                .append(gameHeight, gameState.gameHeight)
                .append(colourPlayed, gameState.colourPlayed)
                .append(gamePieceSet, gameState.gamePieceSet)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(gameWidth)
                .append(gameHeight)
                .append(colourPlayed)
                .append(gamePieceSet)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("gameWidth", gameWidth)
                .append("gameHeight", gameHeight)
                .append("colourPlayed", colourPlayed)
                .append("gamePieceSet", gamePieceSet)
                .toString();
    }

    /**
     * Builder for the GameState class
     */
    public static class GameStateBuilder {

        private int gameWidth;

        private int gameHeight;

        private PlayerColour colourPlayed;

        private Set<GamePiece> gamePieceSet;

        public GameState build() {
            return new GameState(this);
        }

        public GameStateBuilder withGameWidth(final int gameWidth) {
            this.gameWidth = gameWidth;
            return this;
        }

        public GameStateBuilder withGameHeight(final int gameHeight) {
            this.gameHeight = gameHeight;
            return this;
        }

        public GameStateBuilder withColourPlayed(final PlayerColour colourPlayed) {
            this.colourPlayed = colourPlayed;
            return this;
        }

        public GameStateBuilder withGamePieceSet(final Set<GamePiece> gamePieceSet) {
            this.gamePieceSet = gamePieceSet;
            return this;
        }

    }
}
