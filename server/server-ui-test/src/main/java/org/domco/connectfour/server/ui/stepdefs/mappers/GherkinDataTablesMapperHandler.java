package org.domco.connectfour.server.ui.stepdefs.mappers;

import com.google.common.collect.ImmutableList;
import cucumber.api.DataTable;
import org.apache.commons.lang3.StringUtils;
import org.domco.connectfour.server.ui.constants.Constant;
import org.domco.connectfour.server.ui.domain.*;
import org.domco.connectfour.server.ui.domain.Game.GameBuilder;
import org.domco.connectfour.server.ui.domain.GamePageData.GamePageDataBuilder;
import org.domco.connectfour.server.ui.domain.GamePiece.GamePieceBuilder;
import org.domco.connectfour.server.ui.domain.GamePlayer.GamePlayerBuilder;
import org.domco.connectfour.server.ui.domain.GameState.GameStateBuilder;
import org.domco.connectfour.server.ui.domain.HomePageData.HomePageDataBuilder;
import org.domco.connectfour.server.ui.domain.UiPageData.UiPageDataBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Gherkin data tables Mapper Handler.
 */
@Component
public class GherkinDataTablesMapperHandler implements GherkinDataTablesMapper {

    private static final Logger logger = LoggerFactory.getLogger(GherkinDataTablesMapperHandler.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Game mapToExpectedGame(DataTable expectedGameStateDataTable) {
        //No Game row information has been supplied in the test so a game has not begun
        Set<GamePiece> gamePieceSet = new HashSet<>();
        String waitingMessage = Constant.WAITING_MESSAGE;
        Game expectedGame = mapToExpectedGame(expectedGameStateDataTable, gamePieceSet, waitingMessage);

        return expectedGame;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Game mapToExpectedGame(DataTable expectedGameStateDataTable, DataTable expectedGameGridDataTable) {

        //Create set of expected pieces for both players
        List<GamePiece> gamePieces = mapGameGridDataTable(expectedGameGridDataTable);
        Set<GamePiece> gamePieceSet = new HashSet<>(gamePieces);
        String waitingMessage = Constant.DROP_MESSAGE;
        Game expectedGame = mapToExpectedGame(expectedGameStateDataTable, gamePieceSet, waitingMessage);

        return expectedGame;

    }

    /**
     * @param expectedGameStateDataTable state of the game including player information, height, width and who just played a piece in the gherkin data table
     * @param gamePieceSet               expected set of game pieces
     * @return expected game state object for test assertion
     */
    private Game mapToExpectedGame(DataTable expectedGameStateDataTable, Set<GamePiece> gamePieceSet,
                                   String waitingMessage) {

        logger.debug("Attempting to map game state information and game row information to the expected game object ");

        //Check for missing or unrecognised data table column headings from the feature file
        checkForMissingOrUnknownTableHeadings(expectedGameStateDataTable);

        //Create expectedHomePageData for both players
        //Fields on the home and game page object that are constant and do not change.
        String homeAndGamePageTitle = Constant.TITLE_TEXT;
        String message = waitingMessage;

        HomePageData expectedHomePageData = new HomePageDataBuilder()
                .withConnectFourTitle(homeAndGamePageTitle)
                .build();

        //Create expectedYellowGamePageData
        GamePageData expectedYellowGamePageData = new GamePageDataBuilder()
                .withConnectFourTitle(homeAndGamePageTitle)
                .withWaitingMessage(message)
                //Don't build player Id as don't know the guid that is randomly generated
                .build();

        //Create expectedYellowUiDataPage
        UiPageData expectedYellowUiPageData = new UiPageDataBuilder()
                .withHomePageData(expectedHomePageData)
                .withGamePageData(expectedYellowGamePageData)
                .build();

        //Create expectedRedGamePageData
        GamePageData expectedRedGamePageData = new GamePageDataBuilder()
                .withConnectFourTitle(homeAndGamePageTitle)
                .withWaitingMessage(message)
                //Don't build player Id as don't know the guid that is randomly generated
                .build();

        //Create expectedRedUiDataPage
        UiPageData expectedRedUiPageData = new UiPageDataBuilder()
                .withHomePageData(expectedHomePageData)
                .withGamePageData(expectedRedGamePageData)
                .build();

        List<Map<String, String>> playerDataMapList = expectedGameStateDataTable.asMaps(String.class, String.class);

        //Create expected game state for both players - This should be the same
        PlayerColour colourJustPlayed = mapColourJustPlayed(playerDataMapList.get(0).get(Constant.COLOUR_JUST_PLAYED));
        GameState expectedGameState = new GameStateBuilder()
                .withGameHeight(Integer.parseInt(playerDataMapList.get(0).get(Constant.GAME_HEIGHT)))
                .withGameWidth(Integer.parseInt(playerDataMapList.get(0).get(Constant.GAME_WIDTH)))
                .withGamePieceSet(gamePieceSet)
                .withColourPlayed(colourJustPlayed)
                .build();

        //Create expected player object for all players in the test
        List<GamePlayer> expectedPlayers = new ArrayList<>();

        //For each player in the expectedGateStateDataTable
        for (int i = 0; i < playerDataMapList.size(); i++) {

            UiPageData expectedUiPageData;
            if (StringUtils.equals(playerDataMapList.get(i).get(Constant.PLAYER_COLOUR), Constant.YELLOW) ||
                    StringUtils.equals(playerDataMapList.get(i).get(Constant.PLAYER_COLOUR), Constant.UNDEFINED)) {
                expectedUiPageData = expectedYellowUiPageData;
            } else if (StringUtils.equals(playerDataMapList.get(i).get(Constant.PLAYER_COLOUR), Constant.RED)) {
                expectedUiPageData = expectedRedUiPageData;
            } else {
                throw new IllegalStateException("Player colour was not defined as either " + Constant.RED + " , "
                        + Constant.YELLOW + " or " + Constant.UNDEFINED);
            }

            PlayerColour playerColour = mapPlayerColour(playerDataMapList.get(i).get(Constant.PLAYER_COLOUR));
            GameStatus playerGameStatus = getGameStatus(playerDataMapList.get(i).get(Constant.GAME_STATUS));

            GamePlayer expectedPlayer = new GamePlayerBuilder()
                    .withPlayerName(playerDataMapList.get(i).get(Constant.PLAYER_NAME))
                    .withPlayerColour(playerColour)
                    .withOpponentName(playerDataMapList.get(i).get(Constant.OPPONENT_NAME))
                    .withGameState(expectedGameState)
                    .withGameStatus(playerGameStatus)
                    .withUiPageData(expectedUiPageData)
                    .build();

            expectedPlayers.add(expectedPlayer);

        }

        //create expected game
        Game expectedGame = new GameBuilder()
                .withGamePlayerList(expectedPlayers)
                .build();

        logger.debug("Returning mapped expected game object.");
        return expectedGame;
    }

    /**
     * Maps the player colour string from the table to the PlayerColour enum
     *
     * @param colour String specifying the player colour as in the game state gherkin table in the feature file
     * @return PlayerColour enum
     */
    private PlayerColour mapPlayerColour(String colour) {
        PlayerColour playerColour;
        if (StringUtils.equals(colour, Constant.YELLOW)) {
            playerColour = PlayerColour.YELLOW;
        } else if (StringUtils.equals(colour, Constant.RED)) {
            playerColour = PlayerColour.RED;
        } else if (StringUtils.equals(colour, Constant.UNDEFINED)) {
            playerColour = PlayerColour.UNDEFINED;
        } else {
            logger.debug("playerColour was not entered as " + Constant.RED + ", " + Constant.YELLOW + " or " + Constant.UNDEFINED
                    + " in the game state gherkin table");
            throw new IllegalStateException("playerColour was not specified using the correct format in the game state gherkin table");
        }
        return playerColour;
    }

    /**
     * Maps the game status string from the table to the GameStatus enum
     *
     * @param gameStatus as specified in the game status gherkin data table in the feature file
     * @return GameStatus enum
     */
    private GameStatus getGameStatus(String gameStatus) {
        GameStatus playerGameStatus;
        if (StringUtils.equals(gameStatus, Constant.YELLOW_WIN)) {
            playerGameStatus = GameStatus.YELLOW_WIN;
        } else if (StringUtils.equals(gameStatus, (Constant.RED_WIN))) {
            playerGameStatus = GameStatus.RED_WIN;
        } else if (StringUtils.equals(gameStatus, Constant.UNDETERMINED)) {
            playerGameStatus = GameStatus.UNDETERMINED;
        } else if (StringUtils.equals(gameStatus, Constant.TIE)) {
            playerGameStatus = GameStatus.TIE;
        } else {
            logger.debug("Game status was not entered as " + Constant.RED_WIN + ", " + Constant.YELLOW_WIN + ", "
                    + Constant.UNDETERMINED + " or " + Constant.TIE + " in the game status gherkin data table");
            throw new IllegalStateException("gameStatus was not specified using the correct format in the game status gherkin data table");
        }
        return playerGameStatus;
    }

    /**
     * Maps the colour just played string from the table to the PlayerColour enum
     *
     * @param colour the colour who just played as provided in the expected game state gherkin table
     * @return PlayerColour enum
     */
    private PlayerColour mapColourJustPlayed(String colour) {
        PlayerColour colourJustPlayed;
        if (StringUtils.equals(colour, Constant.RED)) {
            colourJustPlayed = PlayerColour.RED;
        } else if (StringUtils.equals(colour, Constant.YELLOW)) {
            colourJustPlayed = PlayerColour.YELLOW;
        } else {
            logger.debug("The game as not begun so no colour has been played");
            colourJustPlayed = PlayerColour.UNDEFINED;
        }
        return colourJustPlayed;
    }

    /**
     * Maps the gherkin table data into a list of game pieces
     *
     * @param gameGridDataTable Represents the game grid data from the Gherkin data table.
     * @return gamePieces             the list to contain all game pieces that have been specified
     * in the game grid gherkin data table in the feature file
     */
    private List<GamePiece> mapGameGridDataTable(DataTable gameGridDataTable) {

        List<GamePiece> allGamePieces = new ArrayList<>();

        //The table starts with the top row. But we want to iterate from the bottom of the game grid upwards.
        List<List<String>> rowsInCorrectOrder = ImmutableList.copyOf(gameGridDataTable.cells(1)).reverse();

        //Iterate through each row
        for (int rowNum = 0; rowNum < rowsInCorrectOrder.size(); rowNum++) {

            List<String> currentRow = rowsInCorrectOrder.get(rowNum);

            //Iterate through each column on the current row
            for (int columnNum = 0; columnNum < currentRow.size(); columnNum++) {

                String currentColumn = currentRow.get(columnNum);

                PlayerColour playerColour;
                //Only create a gamePiece if there was actually something in this table cell
                if (!StringUtils.equals(currentColumn, "")) {

                    if (StringUtils.equals(currentColumn, Constant.RED_COLUMN_SYMBOL)) {
                        playerColour = PlayerColour.RED;
                    } else if (StringUtils.equals(currentColumn, Constant.YELLOW_COLUMN_SYMBOL)) {
                        playerColour = PlayerColour.YELLOW;
                    } else {
                        logger.debug("Player colour was not entered as a " + Constant.RED_COLUMN_SYMBOL +
                                " or a " + Constant.YELLOW_COLUMN_SYMBOL + " in the game piece table");
                        throw new IllegalStateException("Illegal character used in game piece table");
                    }

                    //Create a game piece for this table cell entry and add to the GamePiece list
                    GamePiece gp = new GamePieceBuilder()
                            .withxPosition(columnNum)
                            .withyPosition(rowNum)
                            .withPieceColour(playerColour)
                            .build();

                    allGamePieces.add(gp);

                } else {
                    logger.debug("This table cell was empty");
                }
            }
        }
        return allGamePieces;
    }

    /**
     * Takes the game gird data table from the feature file and verifies all expected headings are present.
     * Additionally it will check to make sure any unrecognised headings are not present.
     * An IllegalStateException will be thrown if headings are missing or unrecognised headings are present.
     *
     * @param expectedGameStateDataTable The data table that has been written in the feature file and
     *                                   serves as the expected game state at the end of the test
     */
    private void checkForMissingOrUnknownTableHeadings(DataTable expectedGameStateDataTable) {

        //Headings that have been written on the expected game state data table from the feature file
        List<String> featureFileTableHeadings = expectedGameStateDataTable.topCells();

        //A list to contain the table headings in the feature files that are valid.
        List<String> validFeatureFileGameStateTableHeadings = new ArrayList<>();

        //Checking all feature file table headings are a correct heading (present in the gameStateDataTableHeadingValues)
        //and that the list of feature file headings is the same size as the list of expected table headings.
        // These two checks will ensure the two lists contain exactly the same headings independent of order.
        for (String currentGameStateTableHeading : featureFileTableHeadings) {

            if (!Constant.gameStateDataTableHeadingValues.contains(currentGameStateTableHeading)) {
                throw new IllegalStateException(currentGameStateTableHeading
                        + " is an unrecognised table heading in the feature file and does not match any of the following values: "
                        + Constant.gameStateDataTableHeadingValues.toString()
                        + " Only these values should be present as column headings in the expected game state data table.");
            }
            validFeatureFileGameStateTableHeadings.add(currentGameStateTableHeading);
        }

        if (validFeatureFileGameStateTableHeadings.size() != (Constant.gameStateDataTableHeadingValues.size())) {
            throw new IllegalStateException("The expected game state table headings provided in the feature file did " +
                    "not contains all necessary values. All of the following headings must be present: "
                    + Constant.gameStateDataTableHeadingValues.toString());
        }

    }

}
