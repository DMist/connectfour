package org.domco.connectfour.server.ui.stepdefs.mappers;

import cucumber.api.DataTable;
import org.domco.connectfour.server.ui.domain.Game;

/**
 * Gherkin data tables Mapper Interface.
 */
public interface GherkinDataTablesMapper {

    /**
     * No expectedGameGridDataTable is passed in as the game has not begun.
     *
     * @param expectedGameStateDataTable the state of the game including player information, height, width and who just played a piece in the gherkin data table
     * @return expected game object for assertion at the end of a test
     */
    Game mapToExpectedGame(DataTable expectedGameStateDataTable);


    /**
     * @param expectedGameStateDataTable the state of the game including player information, height, width and who just played a piece in the gherkin data table
     * @param expectedGameGridDataTable  the pieces played on the gherkin data table in the feature file.
     * @return expected game object for assertion at the end of a test
     */
    Game mapToExpectedGame(DataTable expectedGameStateDataTable, DataTable expectedGameGridDataTable);

}
