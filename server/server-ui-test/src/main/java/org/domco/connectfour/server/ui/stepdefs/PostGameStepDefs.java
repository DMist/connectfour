package org.domco.connectfour.server.ui.stepdefs;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.domco.connectfour.server.ui.domain.Game;
import org.domco.connectfour.server.ui.service.GamePlayService;
import org.domco.connectfour.server.ui.stepdefs.mappers.GherkinDataTablesMapper;

import javax.inject.Inject;

public class PostGameStepDefs {

    private DataTable expectedGameStateDataTable;

    private final GherkinDataTablesMapper gherkinDataTablesMapper;
    private final GamePlayService gamePlayService;

    @Inject
    public PostGameStepDefs(GherkinDataTablesMapper gherkinDataTablesMapper, GamePlayService gamePlayService) {
        this.gherkinDataTablesMapper = gherkinDataTablesMapper;
        this.gamePlayService = gamePlayService;
    }

    @Then("^the game has the following game state information:$")
    public void theGameIsOverWithTheFollowingGameStateInformation(DataTable expectedGameStateDataTable) {

        this.expectedGameStateDataTable = expectedGameStateDataTable;
    }

    @And("^the following pieces were played:$")
    public void theFollowingPiecesWerePlayed(DataTable expectedGameGridDataTable) {
        Game expectedGame = gherkinDataTablesMapper.mapToExpectedGame(expectedGameStateDataTable, expectedGameGridDataTable);
        gamePlayService.assertExpectedGameMatchesActualGame(expectedGame);

    }

}
