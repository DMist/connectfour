package org.domco.connectfour.server.ui.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Game Player.
 */
public final class GamePlayer {

    private String playerName;

    private final PlayerColour playerColour;

    private String opponentName;

    private final GameState gameState;

    private final GameStatus gameStatus;

    //Each player stores information provided via their instance of the UI pages
    private UiPageData uiPageData;

    private GamePlayer(final GamePlayerBuilder builder) {
        this.playerName = builder.playerName;
        this.playerColour = builder.playerColour;
        this.opponentName = builder.opponentName;
        this.gameState = builder.gameState;
        this.gameStatus = builder.gameStatus;
        this.uiPageData = builder.uiPageData;
    }

    public String getPlayerName() {
        return playerName;
    }

    public PlayerColour getPlayerColour() {
        return playerColour;
    }

    public String getOpponentName() {
        return opponentName;
    }

    public GameState getGameState() {
        return gameState;
    }

    public GameStatus getGameStatus() {
        return gameStatus;
    }

    public UiPageData getUiPageData() {
        return uiPageData;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public void setOpponentName(String opponentName) {
        this.opponentName = opponentName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GamePlayer that = (GamePlayer) o;

        return new EqualsBuilder()
                .append(playerName, that.playerName)
                .append(playerColour, that.playerColour)
                .append(opponentName, that.opponentName)
                .append(gameState, that.gameState)
                .append(gameStatus, that.gameStatus)
                .append(uiPageData, that.uiPageData)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(playerName)
                .append(playerColour)
                .append(opponentName)
                .append(gameState)
                .append(gameStatus)
                .append(uiPageData)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("playerName", playerName)
                .append("playerColour", playerColour)
                .append("opponentName", opponentName)
                .append("gameState", gameState)
                .append("gameStatus", gameStatus)
                .append("uiPageData", uiPageData)
                .toString();
    }

    /**
     * Builder for the GamePlayer class
     */
    public static class GamePlayerBuilder {

        private String playerName;

        private PlayerColour playerColour;

        private String opponentName;

        private GameState gameState;

        private GameStatus gameStatus;

        private UiPageData uiPageData;

        public GamePlayer build() {
            return new GamePlayer(this);
        }

        public GamePlayerBuilder withPlayerName(final String playerName) {
            this.playerName = playerName;
            return this;
        }

        public GamePlayerBuilder withPlayerColour(final PlayerColour playerColour) {
            this.playerColour = playerColour;
            return this;
        }

        public GamePlayerBuilder withOpponentName(final String opponentName) {
            this.opponentName = opponentName;
            return this;
        }

        public GamePlayerBuilder withGameState(final GameState gameState) {
            this.gameState = gameState;
            return this;
        }

        public GamePlayerBuilder withGameStatus(final GameStatus gameStatus) {
            this.gameStatus = gameStatus;
            return this;
        }

        public GamePlayerBuilder withUiPageData(final UiPageData uiPageData) {
            this.uiPageData = uiPageData;
            return this;
        }

    }
}
