package org.domco.connectfour.server.ui.presentation.service;

import org.domco.connectfour.server.ui.domain.PlayerColour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Wait;

/**
 * Interface for the browser interaction service.
 */
public interface BrowserInteractionService {

    /**
     * Creates a new browser tab for another player to interact with
     */
    void newBrowserTab();

    /**
     * Switches to the specified players tab in the browser
     *
     * @param playerColour colour of the players tab you want to switch to
     */
    void switchTabFocus(PlayerColour playerColour);

    /**
     * Getter for the driver
     */
    WebDriver getDriver();

    /**
     * @param driver Sets the driver for interaction with the browser
     */
    void setDriver(WebDriver driver);

    /**
     * Getter for the Wait
     */
    Wait<WebDriver> getWait();

    /**
     * @param wait Sets the wait for interaction with the browser
     */
    void setWait(Wait<WebDriver> wait);

}
