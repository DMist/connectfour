package org.domco.connectfour.server.ui.service;

import org.domco.connectfour.server.ui.domain.Game;
import org.domco.connectfour.server.ui.domain.GamePlayer;
import org.domco.connectfour.server.ui.domain.HomePageData;
import org.domco.connectfour.server.ui.domain.UiPageData.UiPageDataBuilder;
import org.domco.connectfour.server.ui.presentation.service.GameInitiationPresentationService;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;

/**
 * Implementation for Game Initiation service
 */
@Component
public class GameInitiationServiceHandler implements GameInitiationService {

    private static final Logger logger = LoggerFactory.getLogger(GameInitiationServiceHandler.class);

    private GameInitiationPresentationService gameInitiationPresentationService;

    private TestStateContainer testStateContainer;

    @Inject
    public GameInitiationServiceHandler(TestStateContainer testStateContainer, GameInitiationPresentationService gameInitiationPresentationService) {
        this.testStateContainer = testStateContainer;
        this.gameInitiationPresentationService = gameInitiationPresentationService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GamePlayer openHomePage(GamePlayer gamePlayer) {

        logger.debug("Opening the home page for " + gamePlayer.getPlayerName());

        HomePageData homePageData = gameInitiationPresentationService.openHomePage(gamePlayer);

        //updating game player object with the home page data.
        GamePlayer updatedGamePlayer = new GamePlayer.GamePlayerBuilder()
                .withPlayerName(gamePlayer.getPlayerName())
                .withPlayerColour(gamePlayer.getPlayerColour())
                .withUiPageData(new UiPageDataBuilder().withHomePageData(homePageData).build())
                .build();

        testStateContainer.addUpdatedPlayerToGameObject(gamePlayer, updatedGamePlayer);

        logger.debug("Returning the updated GamePlayer object after their home page has been opened.");
        return updatedGamePlayer;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void requestToPlayAGame(GamePlayer gamePlayer) {

        logger.debug("Attempting to fill in user information for " + gamePlayer.getPlayerName());

        GamePlayer updatedGamePlayer = gameInitiationPresentationService.requestToPlayAGame(gamePlayer);

        testStateContainer.addUpdatedPlayerToGameObject(gamePlayer, updatedGamePlayer);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void bothPlayersJoinToInitiateGame() {

        logger.debug("Attempting to initiate game between both players");
        GamePlayer yellowGamePlayer = testStateContainer.getGame().getGamePlayerList().get(0);
        GamePlayer redGamePlayer = testStateContainer.getGame().getGamePlayerList().get(1);

        //Update each GamePlayer object once they have both navigated to the home page.
        yellowGamePlayer = openHomePage(yellowGamePlayer);
        redGamePlayer = openHomePage(redGamePlayer);

        List<GamePlayer> updatedGamePlayers = gameInitiationPresentationService
                .bothPlayersJoinToInitiateGame(yellowGamePlayer, redGamePlayer);

        //Updating game object to reflect updated players.
        testStateContainer.addUpdatedPlayerToGameObject(yellowGamePlayer, updatedGamePlayers.get(0));
        testStateContainer.addUpdatedPlayerToGameObject(redGamePlayer, updatedGamePlayers.get(1));

        logger.debug("Updated Game object with the updated players");

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void assertUserHasToWaitForOpponent(Game expectedGame) {

        logger.debug("Performing a check to ensure a player has to be matched with an opponent for the game to commence");

        //Actual game object
        Game actualGame = testStateContainer.getGame();


        //Take player name out of comparison as not displayed on the UI until a game begins
        //Take playerID out of comparison as there is no way to know the UUID before the test
        expectedGame.getGamePlayerList().get(0).setPlayerName(
                testStateContainer.getGame().getGamePlayerList().get(0).getPlayerName());
        expectedGame.getGamePlayerList().get(0).getUiPageData().getGamePageData().setPlayerId(
                testStateContainer.getGame().getGamePlayerList().get(0).getUiPageData().getGamePageData().getPlayerId());

        //Perform assertion.
        Assert.assertEquals(expectedGame, testStateContainer.getGame());

        //Now the playerId format needs to be checked as it was taken out of the initial the comparison
        String uuidRegex = "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}";
        Assert.assertTrue(actualGame.getGamePlayerList().get(0).getUiPageData()
                .getGamePageData().getPlayerId().matches(uuidRegex));

    }

}
