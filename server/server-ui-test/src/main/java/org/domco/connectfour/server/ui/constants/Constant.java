package org.domco.connectfour.server.ui.constants;

import java.util.Arrays;
import java.util.List;

/**
 * Class for all constants of the connect4 application
 */
public class Constant {

    public final static String BASE_URI = "http://localhost:8080";

    public final static String RED = "red";
    public final static String YELLOW = "yellow";
    public final static String UNDEFINED = "undefined";

    public final static String YELLOW_WIN = "yellowWin";
    public final static String RED_WIN = "redWin";
    public final static String UNDETERMINED = "undetermined";
    public final static String TIE = "tie";

    public final static String RED_COLUMN_SYMBOL = "R";
    public final static String YELLOW_COLUMN_SYMBOL = "Y";

    public final static String TITLE_TEXT = "Connect Four.";
    public final static String WAITING_MESSAGE = "Waiting for a game...";
    public final static String DROP_MESSAGE = "Drop Drop Drop Drop Drop Drop Drop";

    //Heading values for gherkin tables
    public final static String PLAYER_NAME = "playerName";
    public final static String OPPONENT_NAME = "opponentName";
    public final static String PLAYER_COLOUR = "playerColour";
    public final static String COLOUR_JUST_PLAYED = "colourJustPlayed";
    public final static String GAME_STATUS = "gameStatus";
    public final static String GAME_WIDTH = "gameWidth";
    public final static String GAME_HEIGHT = "gameHeight";

    //List of the allowed and expected heading values for the expected game state gherkin data table.
    public final static List<String> gameStateDataTableHeadingValues = Arrays.asList(PLAYER_NAME, OPPONENT_NAME, PLAYER_COLOUR,
            COLOUR_JUST_PLAYED, GAME_STATUS, GAME_WIDTH, GAME_HEIGHT);


}
