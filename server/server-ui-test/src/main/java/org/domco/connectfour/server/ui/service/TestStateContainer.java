package org.domco.connectfour.server.ui.service;

import org.domco.connectfour.server.ui.domain.Game;
import org.domco.connectfour.server.ui.domain.GamePlayer;

/**
 * Interface for the State of the application throughout each test.
 */
public interface TestStateContainer {

    /**
     * @return The Game object to contain the state of the game throughout the test.
     */
    Game getGame();

    /**
     * Sets the Game object to contain the state of the game throughout the test.
     */
    void setGame(Game game);

    /**
     * Replaces the outdated player object with an updated one that
     * reflects the state of the player at this point in the game
     *
     * @param gamePlayer        the player object that does not reflect the new action that has been performed in the test
     * @param updatedGamePlayer the player object that is up-to-date with the state of the player at this point in the game
     */
    void addUpdatedPlayerToGameObject(GamePlayer gamePlayer, GamePlayer updatedGamePlayer);
}
