package org.domco.connectfour.server.ui.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Domain object for the corresponding Home Page object.
 */
public class HomePageData {

    private final String connectFourTitle;

    private HomePageData(final HomePageDataBuilder builder) {
        this.connectFourTitle = builder.connectFourTitle;
    }

    public String getConnectFourTitle() {
        return connectFourTitle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        HomePageData homePageData = (HomePageData) o;

        return new EqualsBuilder()
                .append(connectFourTitle, homePageData.connectFourTitle)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(connectFourTitle)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("connectFourTitle", connectFourTitle)
                .toString();
    }

    /**
     * Builder for the HomePageData class
     */
    public static class HomePageDataBuilder {

        private String connectFourTitle;

        public HomePageData build() {
            return new HomePageData(this);
        }

        public HomePageDataBuilder withConnectFourTitle(final String connectFourTitle) {
            this.connectFourTitle = connectFourTitle;
            return this;
        }

    }

}
