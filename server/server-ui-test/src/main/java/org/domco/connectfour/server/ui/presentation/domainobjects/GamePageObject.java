package org.domco.connectfour.server.ui.presentation.domainobjects;

import com.frameworkium.core.ui.annotations.Visible;
import com.frameworkium.core.ui.pages.BasePage;
import org.domco.connectfour.server.ui.presentation.domainobjects.components.GameGrid;
import org.domco.connectfour.server.ui.presentation.domainobjects.components.GameInformation;
import org.domco.connectfour.server.ui.presentation.domainobjects.components.TitleBar;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Page Object of the Game Page
 */
public class GamePageObject extends BasePage<GamePageObject> {

    //Title bar component
    @Visible
    private TitleBar titleBar;

    //Game information component
    @Visible
    private GameInformation gameInformation;

    //Game grid component
    private GameGrid gameGrid;

    /**
     * The playerId is stored in the url when a player clicks "Play a Game"
     *
     * @return the playerId of the Player who's Game Page object has been passed in
     */
    public String fetchPlayerId() {
        String playerId;
        Matcher playerIdMatcher = Pattern.compile("playerId=(.*)").matcher(driver.getCurrentUrl());
        if (playerIdMatcher.find()) {
            playerId = playerIdMatcher.group(1);
        } else {
            logger.debug("PlayerId can not be located. Player may not have registered to play.");
            throw new IllegalStateException("The playerId can't be found in the url");
        }
        return playerId;
    }

    /**
     * The isDisplayed() method throws a "no such element exception" is it is not displayed.
     * This method catches that exception so a boolean is returned depending on whether the element is present.
     *
     * @param webElement representation for html element
     * @return boolean value for if the webElement is displayed
     */
    public boolean isVisible(WebElement webElement) {
        boolean isVisible = false;
        try {
            webElement.isDisplayed();
            isVisible = true;
        } catch (NoSuchElementException e) {
            logger.debug(webElement + " is not visible.");
        }
        return isVisible;
    }

    public TitleBar getTitleBar() {
        return titleBar;
    }

    public GameInformation getGameInformation() {
        return gameInformation;
    }

    public GameGrid getGameGrid() {
        return gameGrid;
    }

}
