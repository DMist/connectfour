Feature: Two users can player a game until one of them wins.

  Scenario: When two users are in a game one of them can end the game with a vertical win

    Given two users "Jez" and "Nick" have chosen to join a game
    When "Nick" drops a piece in column 0
    And "Jez" drops a piece in column 1
    And "Nick" drops a piece in column 0
    And "Jez" drops a piece in column 2
    And "Nick" drops a piece in column 0
    And "Jez" drops a piece in column 3
    And "Nick" drops a piece in column 0
    Then the game has the following game state information:
      | playerName | playerColour | opponentName | gameStatus | gameHeight | gameWidth | colourJustPlayed |
      | Jez        | yellow       | Nick         | redWin     | 6          | 7         | red              |
      | Nick       | red          | Jez          | redWin     | 6          | 7         | red              |
    And the following pieces were played:
      | c0 | c1 | c2 | c3 | c4 | c5 | c6 |
      |    |    |    |    |    |    |    |
      |    |    |    |    |    |    |    |
      | R  |    |    |    |    |    |    |
      | R  |    |    |    |    |    |    |
      | R  |    |    |    |    |    |    |
      | R  | Y  | Y  | Y  |    |    |    |

  Scenario: When two users are in a game one of them can end the game with a horizontal win

    Given two users "Dave" and "Terry" have chosen to join a game
    When "Terry" drops a piece in column 0
    And "Dave" drops a piece in column 1
    And "Terry" drops a piece in column 0
    And "Dave" drops a piece in column 0
    And "Terry" drops a piece in column 1
    And "Dave" drops a piece in column 2
    And "Terry" drops a piece in column 2
    And "Dave" drops a piece in column 3
    And "Terry" drops a piece in column 3
    Then the game has the following game state information:
      | playerName | playerColour | opponentName | gameStatus | gameHeight | gameWidth | colourJustPlayed |
      | Dave       | yellow       | Terry        | redWin     | 6          | 7         | red              |
      | Terry      | red          | Dave         | redWin     | 6          | 7         | red              |
    And the following pieces were played:
      | c0 | c1 | c2 | c3 | c4 | c5 | c6 |
      |    |    |    |    |    |    |    |
      |    |    |    |    |    |    |    |
      |    |    |    |    |    |    |    |
      | Y  |    |    |    |    |    |    |
      | R  | R  | R  | R  |    |    |    |
      | R  | Y  | Y  | Y  |    |    |    |

  Scenario: When two users are in a game one of them can end the game with a diagonal win

    Given two users "Sarah" and "Jill" have chosen to join a game
    When "Jill" drops a piece in column 3
    And "Sarah" drops a piece in column 0
    And "Jill" drops a piece in column 1
    And "Sarah" drops a piece in column 1
    And "Jill" drops a piece in column 2
    And "Sarah" drops a piece in column 2
    And "Jill" drops a piece in column 3
    And "Sarah" drops a piece in column 2
    And "Jill" drops a piece in column 3
    And "Sarah" drops a piece in column 3

    Then the game has the following game state information:
      | playerName | playerColour | opponentName | gameStatus | gameHeight | gameWidth | colourJustPlayed |
      | Sarah      | yellow       | Jill         | yellowWin  | 6          | 7         | yellow           |
      | Jill       | red          | Sarah        | yellowWin  | 6          | 7         | yellow           |
    And the following pieces were played:
      | c0 | c1 | c2 | c3 | c4 | c5 | c6 |
      |    |    |    |    |    |    |    |
      |    |    |    |    |    |    |    |
      |    |    |    | Y  |    |    |    |
      |    |    | Y  | R  |    |    |    |
      |    | Y  | Y  | R  |    |    |    |
      | Y  | R  | R  | R  |    |    |    |