Feature: Two users can player a game until one of them wins.

  Scenario: When two users are in a game one of them can end the game with a vertical win
    Given two users "Ralph" and "Chief Wiggum" have chosen to join a game
    When "Chief Wiggum" drops a piece in column 6
    And "Ralph" drops a piece in column 1
    And "Chief Wiggum" drops a piece in column 6
    And "Ralph" drops a piece in column 2
    And "Chief Wiggum" drops a piece in column 6
    And "Ralph" drops a piece in column 3
    And "Chief Wiggum" drops a piece in column 6
    Then a game is created for the players with the following states
      | playerName   | opponentName | playerColour | opponentColour | turnColour | gameStatus | winner       | gameWidth | gameHeight |
      | Ralph        | Chief Wiggum | yellow       | red            | yellow     | redWin     | Chief Wiggum | 7         | 6          |
      | Chief Wiggum | Ralph        | red          | yellow         | yellow     | redWin     | Chief Wiggum | 7         | 6          |
    And the following pieces were played:
      | c0 | c1 | c2 | c3 | c4 | c5 | c6 |
      |    |    |    |    |    |    |    |
      |    |    |    |    |    |    |    |
      |    |    |    |    |    |    | R  |
      |    |    |    |    |    |    | R  |
      |    |    |    |    |    |    | R  |
      |    | Y  | Y  | Y  |    |    | R  |