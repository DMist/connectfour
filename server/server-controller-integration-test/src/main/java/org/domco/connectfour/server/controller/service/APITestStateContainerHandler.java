package org.domco.connectfour.server.controller.service;

import org.domco.connectfour.server.controller.domain.Game;
import org.domco.connectfour.server.controller.domain.PlayerGameState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Implementation of interface for the State of the game throughout each test.
 */
@Component
public class APITestStateContainerHandler implements APITestStateContainer {

    private static final Logger logger = LoggerFactory.getLogger(APITestStateContainerHandler.class);

    //The Game object to contain the state of the game throughout the test.
    private Game game;

    /**
     * {@inheritDoc}
     */
    @Override
    public Game getGame() {
        return game;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setGame(Game game) {
        this.game = game;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addUpdatedPlayerToGameObject(PlayerGameState playerGameState, PlayerGameState updatedPlayerGameState) {

        logger.debug("Attempting to update Game object with the updated player game state");

        //Identify old playerGameState object that needs to be replaced in the game list of players.
        Optional<PlayerGameState> playerGameStateOptional = getGame().getPlayerGameStateList()
                .stream()
                .filter(currentPlayerGameState -> currentPlayerGameState.getPlayerColour().equals(playerGameState.getPlayerColour()))
                .findFirst();

        //Replace old playerGameState object with updated player object.
        if (playerGameStateOptional.isPresent()) {
            PlayerGameState playerGameStateToReplace = playerGameStateOptional.get();
            getGame().getPlayerGameStateList()
                    .set(getGame().getPlayerGameStateList().indexOf(playerGameStateToReplace), updatedPlayerGameState);
        } else {
            throw new IllegalStateException("Old playerGameState object to be replaced could not be found in the list of playerGameStates.");
        }

    }
}
