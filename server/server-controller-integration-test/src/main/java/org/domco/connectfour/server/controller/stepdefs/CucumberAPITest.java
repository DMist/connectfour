package org.domco.connectfour.server.controller.stepdefs;

import cucumber.api.java.Before;
import org.domco.connectfour.server.controller.integration.ApiControllerIntegration;

import javax.inject.Inject;
import javax.management.JMException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;

/**
 * Setup class for API tests to remove test data from previous tests.
 */
public class CucumberAPITest {

    private final ApiControllerIntegration apiControllerIntegration;

    @Inject
    public CucumberAPITest(ApiControllerIntegration apiControllerIntegration) {
        this.apiControllerIntegration = apiControllerIntegration;
    }

    @Before
    public void setup() {
        //Clean up any leftover players from the previous test
        removePreviousTestPlayers();
    }

    /**
     * Manages the JMX agent to wipe all existing players before the test runs.
     */
    private void removePreviousTestPlayers() {

        try {

            final JMXServiceURL url = new JMXServiceURL(
                    "service:jmx:rmi:///jndi/rmi://localhost:9004/jmxrmi");
            final JMXConnector jmxc = JMXConnectorFactory.connect(url, null);

            final MBeanServerConnection mbsx = jmxc.getMBeanServerConnection();

            // Uniquely identify the MBeans and register them with the platform MBeanServer
            final ObjectName playerLookupManagerName = new ObjectName("org.domco.connectfour.server.controller.application.mbean:name=playerLookupManager,type=PlayerLookupManager");

            mbsx.invoke(playerLookupManagerName, "emptyPlayerLookupMap", new Object[0], new String[0]);

        } catch (final JMException | IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
