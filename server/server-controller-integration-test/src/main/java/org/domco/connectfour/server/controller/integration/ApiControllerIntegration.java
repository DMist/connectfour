package org.domco.connectfour.server.controller.integration;

import io.restassured.response.ValidatableResponse;
import org.domco.connectfour.server.controller.domain.PlayerGameState;
import org.domco.connectfour.server.controller.domain.Player;

/**
 * Interface for all integration methods for the Connect4 api
 */
public interface ApiControllerIntegration {

    /**
     * Creates playerId for a given player by making them join a game
     *
     * @param playerName name of the player to join the game and receive the playerId
     * @return The player that has just joined the game and received the playerId
     */
    String playerRequestsToJoinGame(String playerName);

    /**
     * For a given player retrieve the PlayerGameState of their Game
     *
     * @param playerId the player id of the player we are retrieving the state of
     * @return PlayerGameState for the given player
     */
    PlayerGameState retrievePlayersGameState(String playerId);

    /**
     * Plays a piece in the chosen column
     *
     * @param playerId  ID of the player who is attempting to perform the move
     * @param columnNum number of the column the player wants to play their piece in
     * @return The updated PlayerGameState for the given player
     */
    PlayerGameState playGamePiece(String playerId, int columnNum);

}
