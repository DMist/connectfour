package org.domco.connectfour.server.controller.stepdefs;

import cucumber.api.DataTable;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.domco.connectfour.server.controller.constants.Constant;
import org.domco.connectfour.server.controller.domain.Game;
import org.domco.connectfour.server.controller.domain.Player;
import org.domco.connectfour.server.controller.service.APITestStateContainer;
import org.domco.connectfour.server.controller.service.ApiControllerService;
import org.domco.connectfour.server.controller.stepdefs.mappers.GherkinDataTablesMapper;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Step definitions for the API Controller feature file tests
 */
public class ApiControllerStepDefs {

    private DataTable expectedGameStateDataTable;

    private ApiControllerService apiControllerService;
    private GherkinDataTablesMapper gherkinDataTablesMapper;
    private APITestStateContainer apiTestStateContainer;

    @Inject
    public ApiControllerStepDefs(ApiControllerService apiControllerService, GherkinDataTablesMapper gherkinDataTablesMapper,
                                 APITestStateContainer apiTestStateContainer) {
        this.apiControllerService = apiControllerService;
        this.gherkinDataTablesMapper = gherkinDataTablesMapper;
        this.apiTestStateContainer = apiTestStateContainer;
    }

    @When("^\"([^\"]*)\" tries to join a new game$")
    public void triesToJoinANewGame(String playerName) {
        List<Player> players = new ArrayList<>();
        Player player = apiControllerService.playerRequestsToJoinGame(playerName);
        players.add(player);
        //Store the state of the actual game in the apiTestStateContainer
        apiTestStateContainer.setGame(apiControllerService.retrievePlayersGameStates(players));
    }

    @Given("^two users \"([^\"]*)\" and \"([^\"]*)\" have chosen to join a game$")
    public void twoUsersAndHaveChosenToJoinAGame(String yellowPlayer, String redPlayer) {
        List<Player> players = new ArrayList<>();
        players.add(apiControllerService.playerRequestsToJoinGame(yellowPlayer));
        players.add(apiControllerService.playerRequestsToJoinGame(redPlayer));
        //Store the state of the actual game in the apiTestStateContainer
        apiTestStateContainer.setGame(apiControllerService.retrievePlayersGameStates(players));
    }

    @When("^\"([^\"]*)\" drops a piece in column (\\d+)$")
    public void dropsAPieceInColumn(String playerName, int columnNum) {
        apiControllerService.playGamePiece(playerName, columnNum);
    }

    @Then("^\"([^\"]*)\" is given a player ID$")
    public void givenAPlayerID(String playerName) {
        Optional<Player> playerOptional = apiTestStateContainer.getGame().getPlayers().stream()
                .filter(player -> player.getPlayerName().equals(playerName))
                .findFirst();

        if (playerOptional.isPresent()) {
            final Player currentPlayer = playerOptional.get();
            apiControllerService.assertPlayerIdIsPresent(currentPlayer);
        } else {
            throw new IllegalStateException(playerName + " was not found in the list of players.");
        }

    }

    @Then("^a game is created for the players with the following states$")
    public void aGameIsCreatedForThePlayersWithTheFollowingState(DataTable expectedGameStateDataTable) {

        //Check for missing or unrecognised data table column headings from the feature file
        checkForMissingOrUnknownTableHeadings(expectedGameStateDataTable);

        this.expectedGameStateDataTable = expectedGameStateDataTable;
        //Store the state of the actual game in the apiTestStateContainer
        apiTestStateContainer.setGame(apiControllerService.retrievePlayersGameStates(apiTestStateContainer.getGame().getPlayers()));

    }

    @And("^the following pieces were played:$")
    public void theFollowingPiecesWerePlayed(DataTable expectedGameGridDataTable) {
        Game expectedGame = gherkinDataTablesMapper.mapToExpectedGame(expectedGameStateDataTable, expectedGameGridDataTable);
        apiControllerService.assertPlayerGameStates(expectedGame);
    }

    /**
     * Takes the game gird data table from the feature file and verifies all expected headings are present.
     * Additionally it will check to make sure any unrecognised headings are not present.
     * An IllegalStateException will be thrown if headings are missing or unrecognised headings are present.
     *
     * @param expectedGameStateDataTable The data table that has been written in the feature file and
     *                                   serves as the expected game state at the end of the test
     */
    private void checkForMissingOrUnknownTableHeadings(DataTable expectedGameStateDataTable) {

        //Headings that have been written on the expected game state data table from the feature file
        List<String> featureFileTableHeadings = expectedGameStateDataTable.topCells();

        //A list to contain the table headings in the feature files that are valid.
        List<String> validFeatureFileGameStateTableHeadings = new ArrayList<>();

        //Checking all feature file table headings are a correct heading (present in the gameStateDataTableHeadingValues)
        //and that the list of feature file headings is the same size as the list of expected table headings.
        // These two checks will ensure the two lists contain exactly the same headings independent of order.
        for (String currentGameStateTableHeading : featureFileTableHeadings) {

            if (!Constant.gameStateDataTableHeadingValues.contains(currentGameStateTableHeading)) {
                throw new IllegalStateException(currentGameStateTableHeading
                        + " is an unrecognised table heading in the feature file and does not match any of the following values: "
                        + Constant.gameStateDataTableHeadingValues.toString()
                        + " Only these values should be present as column headings in the expected game state data table.");
            }
            validFeatureFileGameStateTableHeadings.add(currentGameStateTableHeading);
        }

        if (validFeatureFileGameStateTableHeadings.size() != (Constant.gameStateDataTableHeadingValues.size())) {
            throw new IllegalStateException("The expected game state table headings provided in the feature file did " +
                    "not contains all necessary values. All of the following headings must be present: "
                    + Constant.gameStateDataTableHeadingValues.toString());
        }

    }
}
