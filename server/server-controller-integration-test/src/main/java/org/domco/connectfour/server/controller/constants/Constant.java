package org.domco.connectfour.server.controller.constants;

import org.domco.connectfour.server.controller.domain.GameStatus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Constants class.
 */
public class Constant {

    public final static String RED = "red";
    public final static String YELLOW = "yellow";
    public final static String UNDEFINED = "undefined";

    public final static String YELLOW_WIN = "yellowWin";
    public final static String RED_WIN = "redWin";
    public final static String UNDETERMINED = "undetermined";
    public final static String TIE = "tie";

    public final static String RED_COLUMN_SYMBOL = "R";
    public final static String YELLOW_COLUMN_SYMBOL = "Y";

    //Heading values for gherkin tables
    public final static String PLAYER_NAME = "playerName";
    public final static String OPPONENT_NAME = "opponentName";
    public final static String PLAYER_COLOUR = "playerColour";
    public final static String OPPONENT_COLOUR = "opponentColour";
    public final static String TURN_COLOUR = "turnColour";
    public final static String GAME_STATUS = "gameStatus";
    public final static String WINNER = "winner";
    public final static String GAME_WIDTH = "gameWidth";
    public final static String GAME_HEIGHT = "gameHeight";

    //List of the allowed and expected heading values for the expected game state gherkin data table.
    public final static List<String> gameStateDataTableHeadingValues = Arrays.asList(PLAYER_NAME, OPPONENT_NAME, PLAYER_COLOUR,
            OPPONENT_COLOUR, TURN_COLOUR, GAME_STATUS, WINNER, GAME_WIDTH, GAME_HEIGHT);

}
