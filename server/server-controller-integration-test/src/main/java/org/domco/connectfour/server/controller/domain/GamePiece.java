package org.domco.connectfour.server.controller.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Game Piece.
 */
public class GamePiece {

    private int xPosition;

    private int yPosition;

    private PlayerColour pieceColour;

    private GamePiece(final GamePieceBuilder builder) {
        this.xPosition = builder.xPosition;
        this.yPosition = builder.yPosition;
        this.pieceColour = builder.pieceColour;
    }

    public int getxPosition() {
        return xPosition;
    }

    public int getyPosition() {
        return yPosition;
    }

    public PlayerColour getPieceColour() {
        return pieceColour;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        GamePiece gamePiece = (GamePiece) o;

        return new EqualsBuilder()
                .append(xPosition, gamePiece.xPosition)
                .append(yPosition, gamePiece.yPosition)
                .append(pieceColour, gamePiece.pieceColour)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(xPosition)
                .append(yPosition)
                .append(pieceColour)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("xPosition", xPosition)
                .append("yPosition", yPosition)
                .append("pieceColour", pieceColour)
                .toString();
    }

    /**
     * Builder for the GamePiece class
     */
    public static class GamePieceBuilder {

        private int xPosition;

        private int yPosition;

        private PlayerColour pieceColour;

        public GamePiece build() {
            return new GamePiece(this);
        }

        public GamePieceBuilder withxPosition(final int xPosition) {
            this.xPosition = xPosition;
            return this;
        }

        public GamePieceBuilder withyPosition(final int yPosition) {
            this.yPosition = yPosition;
            return this;
        }

        public GamePieceBuilder withPieceColour(final PlayerColour pieceColour) {
            this.pieceColour = pieceColour;
            return this;
        }

    }

}
