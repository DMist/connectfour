package org.domco.connectfour.server.controller.constants;

/**
 * The various endpoints of the connect4 application.
 */
public class ConnectFourEndpoint {

    private static final String API = "/api";

    public static final String BASE_URI = "http://localhost:8080/";
    public static final String JOIN_GAME = API + "/join?playerName=%s";
    public static final String RETRIEVE_GAME = API + "/retrieveGame?playerId=%s";
    public static final String PLAY_GAME_PIECE = API + "/playGamePiece?playerId=%s&position=%d";

}
