package org.domco.connectfour.server.controller.application.mbean;

import org.domco.connectfour.server.controller.application.controllers.ApiController;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.HashMap;

/**
 * Implementation of the Player Lookup Manager MBean
 */
@Component
public class PlayerLookupManager implements PlayerLookupManagerMBean {

    private final ApiController apiController;

    /**
     * Constructs player lookup manager.
     *
     * @param apiController API controller.
     */
    @Inject
    public PlayerLookupManager(final ApiController apiController) {
        this.apiController = apiController;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void emptyPlayerLookupMap() {
        apiController.setPlayerLookup(new HashMap<>());
    }
}
